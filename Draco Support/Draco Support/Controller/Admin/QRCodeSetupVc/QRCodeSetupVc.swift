//
//  QRCodeSetupVc.swift
//  Draco Support
//
//  Created by Haresh on 23/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import DropDown

class QRCodeSetupVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var vwCompanyName: CustomView!
    @IBOutlet weak var txtCoompanyName: CustomTextField!  //VwIsHidden From StoryBoard
    
    
    @IBOutlet weak var lblEquipmentName: UILabel!
    @IBOutlet weak var vwEquipmentName: CustomView!
    @IBOutlet weak var txtEquipementName: CustomTextField!
    
    @IBOutlet weak var lblManufacture: UILabel!
    @IBOutlet weak var txtvwManufacture: CustomTextview!
    @IBOutlet weak var vwManufacture: CustomView!
    @IBOutlet weak var lblManufacturePlaceHolder: UILabel!
    
    @IBOutlet weak var lblLocationOfEquipment: UILabel!
    
    @IBOutlet weak var lblLocationEquipmentPlaceholder: UILabel!
    @IBOutlet weak var txtvwLocationOfEquipment: CustomTextview!
    @IBOutlet weak var vwLocationOfEquipment: CustomView!
    
    @IBOutlet weak var lblAddImage: UILabel!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    //MARK: - Variable
    
    var companyNameDD = DropDown()
    var strCompnayID = "1"
    
    var arrayCompanyName : [JSON] = []
    var arrayCompanyNameString = [String]()
    
    var codeString = ""
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        print("codeString : \(codeString)")
        getCompanyOrganizationList()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "QR_Code_Setup_key"))
        
        if (getUserDetail("role") == "2") {
            self.strCompnayID = getUserDetail("company_id")
        }

    }
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.companyNameDD, sender: self.txtCoompanyName)
        selectionIndex()
    }
    
}

//MARK: - SetupUI

extension QRCodeSetupVc
{
    
    func setupUI()
    {
        
        [txtCoompanyName, txtEquipementName].forEach { (txt) in
            txt?.delegate = self
            txt?.leftPaddingView = 15
            txt?.setThemeTextFieldUI()
        }
        
        [vwCompanyName, vwEquipmentName].forEach { (vw) in
            vw?.cornerRadius = (vw!.bounds.height)/2
            vw?.borderWidth = 1.0
            vw?.borderColors = UIColor.appthemeRedColor
            vw?.shadowColors = UIColor.lightGray
            vw?.shadowRadius = 2.0
            vw?.shadowOffset = CGSize(width: 1.0, height: 1.0)
            vw?.shadowOpacity = 0.8
        }
        
        [vwManufacture, vwLocationOfEquipment].forEach { (vw) in
            vw?.cornerRadius = 5
            vw?.borderWidth = 1.0
            vw?.borderColors = UIColor.appthemeRedColor
            vw?.shadowColors = UIColor.lightGray
            vw?.shadowRadius = 2.0
            vw?.shadowOffset = CGSize(width: 1.0, height: 1.0)
            vw?.shadowOpacity = 0.8
        }
        
        
        [txtvwManufacture,txtvwLocationOfEquipment].forEach { (txt) in
            txt?.delegate = self
            txt?.tintColor = UIColor.appthemeRedColor
            txt?.font = themeFont(size: 16, fontname: .regular)
        }
        
        [lblManufacturePlaceHolder,lblLocationEquipmentPlaceholder].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.text = getCommonString(key: "Enter_here_key")
        }
        
        [lblCompanyName,lblEquipmentName,lblManufacture,lblLocationOfEquipment,lblAddImage].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 16, fontname: .regular)
        }
        
        lblCompanyName.text = getCommonString(key: "Company_Name_key")
        lblEquipmentName.text = getCommonString(key: "Equipment_Name_key")
        lblManufacture.text = getCommonString(key: "Manufacture/Model_key")
        lblLocationOfEquipment.text = getCommonString(key: "Location_of_the_Equipment_key")
        lblAddImage.text = getCommonString(key: "Add_Image_key")
        
        btnSubmit.setThemeButtonUI()
        btnSubmit.setTitle(getCommonString(key: "Submit_key"), for: .normal)
        
        self.txtCoompanyName.text = getUserDetail("company_name")
        
//        if getUserDetail("role") == "1" {
//            companyNameDD.show()
//        }
//        else {
////            companyNameDD.dataSource = ["\(getUserDetail("company_name"))"]
//            companyNameDD.hide()
//        }
        
    }
    
}

//MARK: - IBAction Method

extension QRCodeSetupVc
{
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        if (txtCoompanyName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_select_company_key"))
        }
        else if (txtEquipementName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_euipment_name_key"))
        }
        else if (txtvwManufacture.text.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_manufacture/Model_key"))
        }
        else if (txtvwLocationOfEquipment.text.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_location_of_equipment_key"))
        }
        else
        {
            codeSetupAPICalling()
        }
 
    }
    
    func selectionIndex()
    {
        
        self.companyNameDD.selectionAction = { (index, item) in
            self.txtCoompanyName.text = item
            self.view.endEditing(true)
            
            self.strCompnayID = (self.arrayCompanyName[index]["id"]).stringValue
            print("CompanyID : \(self.strCompnayID)")
            self.companyNameDD.hide()
            
        }
    }
    
    func allFieldReset()
    {
        txtCoompanyName.text = ""
        txtEquipementName.text = ""
        txtvwManufacture.text = ""
        txtvwLocationOfEquipment.text = ""
        
        lblManufacturePlaceHolder.isHidden = false
        lblLocationEquipmentPlaceholder.isHidden = false
    }
    
}

//MARK: - textView Delegate

extension QRCodeSetupVc : UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView)
    {
        if textView == txtvwManufacture {
            self.lblManufacturePlaceHolder.isHidden = textView.text == "" ? false : true
        }
        else if textView == txtvwLocationOfEquipment {
            self.lblLocationEquipmentPlaceholder.isHidden = textView.text == "" ? false : true
        }
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        
        return true
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//
//        if textField == txtCoompanyName
//        {
//            self.view.endEditing(true)
//            companyNameDD.show()
//            return false
//        }
//
//        return true
//    }
}

//MARK: - TextField Delegate

extension QRCodeSetupVc: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtCoompanyName
        {
            self.view.endEditing(true)
            companyNameDD.show()
            return false
        }
        
        return true
    }
    
}

//MARK: - API calling

extension QRCodeSetupVc
{
    
    func codeSetupAPICalling()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(issueDetailURL)\(QRCodeSetupURL)"
            
            print("URL: \(url)")
            
            print("codeString:\(codeString)")
            
            let param = ["access_token" : getUserDetail("access_token"),
                         "manufacturer" : txtvwManufacture.text ?? "",
                         "location" : txtvwLocationOfEquipment.text ?? "",
                         "computer_name" : txtEquipementName.text ?? "",
                         "qr_code" : codeString,
                         "user_id" : getUserDetail("id"),
                         "company_id" : self.strCompnayID

            ]
//            "company_id" : "\(self.strCompnayID) ?? \(getUserDetail("company_id"))"
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        
                        makeToast(strMessage: json["msg"].stringValue)
                        
                       self.navigationController?.popToRootViewController(animated: true)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                         self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    
    func getCompanyOrganizationList()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(adminListURL)"
            
            print("URL: \(url)")
            
            let param = [String:String]()
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
                        self.arrayCompanyName = data
                        
                        self.arrayCompanyNameString = data.map({"\($0["company_name"].stringValue)"})
                        
                        if getUserDetail("role") == "1" {
                            self.companyNameDD.dataSource = self.arrayCompanyNameString
                        }
                        else {
                            self.companyNameDD.dataSource = ["\(getUserDetail("company_name"))"]
                        }
//                        self.companyNameDD.dataSource = self.arrayCompanyNameString
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                         self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
 
}
