//
//  AdminSignupVc.swift
//  Draco Support
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import DropDown
import SwiftyJSON
import FirebaseMessaging
import GoogleMaps

class AdminSignupVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var vwFirstName: CustomView!
    @IBOutlet weak var txtfirstName: CustomTextField!
    
    @IBOutlet weak var vwLastName: CustomView!
    @IBOutlet weak var txtLastName: CustomTextField!
    
    @IBOutlet weak var vwPhoneNumber: CustomView!
    @IBOutlet weak var txtPhoneNumber: CustomTextField!
    
    @IBOutlet weak var vwAuthenticationCode: CustomView!
    @IBOutlet weak var txtAuthenticationCode: CustomTextField!
    
    @IBOutlet weak var vwCompanyName: CustomView!
    @IBOutlet weak var txtCompanyName: CustomTextField!
    
    @IBOutlet weak var vwEmail: CustomView!
    @IBOutlet weak var txtEmail: CustomTextField!
    
    @IBOutlet weak var vwPassword: CustomView!
    @IBOutlet weak var txtPassword: CustomTextField!
    
    @IBOutlet weak var vwConfirmPassword: CustomView!
    @IBOutlet weak var txtConfirmPassword: CustomTextField!
    
    
    @IBOutlet weak var btnSignUp: CustomButton!
    
    //MARK: - Variable
    
    var companyNameDD = DropDown()
    var strCompnayID = ""
    
    var arrayCompanyName : [JSON] = []
    var arrayCompanyNameString = [String]()
    
    var arrayBatchList : [JSON] = []
    var arrayBatchListString = [String]()
    var batchListCheck = Bool()
    
    var QRCodeCheck = Bool()
    
    var arrayQRCodeString = [String]()
    var arrayData : [JSON] = []
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        
        getCompanyOrganizationList()
        getCompanyBatchList()
        
//        checkValidEmailDomain()
        print("emailDomainNameString: ----  \(self.arrayBatchListString)")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Draco_Support_key"))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.companyNameDD, sender: self.txtCompanyName)
        selectionIndex()
    }
    
    //MARK: check valid email domain and authentication code
    
    func checkValidEmailDomain() {

        print("emailDomainNameString: ----  \(self.arrayBatchListString)")
        print(self.arrayData)
        
        var dictQRData = JSON()
        
        if arrayData.contains(where: { (json) -> Bool in
            if json["QR_code"].stringValue == txtAuthenticationCode.text?.trimmingCharacters(in: .whitespaces)
            {
                dictQRData = json
                return true
            }
            return false
        })
        {
            print("DictQRData:\(dictQRData)")
            print("Last domain : \(txtEmail.text?.components(separatedBy: "@").last)")
            
            if dictQRData["batch_name"].stringValue == txtEmail.text?.components(separatedBy: "@").last
            {
                self.signUpAPICalling()
                print("valid for registration:")
            }
            else
            {
                print("not valid for registration")
                makeToast(strMessage: getCommonString(key: "Please_enter_valid_email"))
            }
            
        }
        else
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_valid_authentication_code_key"))
            print("QRCode not register:")
        }
        
//        if let validBatchList = txtEmail.text?.components(separatedBy: "@").last, self.arrayBatchListString.contains(validBatchList) {
//
//            self.batchListCheck = true
//           print("Valid Email---------")
//        } else {
//            self.batchListCheck = false
//            print("envalidValid Email---------")
//        }
    }
    
}

//MARK: - SetupUI
extension AdminSignupVc
{
    
    func setupUI()
    {
        [txtfirstName,txtLastName,txtPhoneNumber,txtCompanyName,txtEmail,txtPassword,txtConfirmPassword,txtAuthenticationCode].forEach { (txt) in
            txt?.delegate = self
            txt?.leftPaddingView = 55
            txt?.setThemeTextFieldUI()
            
        }

        [vwFirstName,vwLastName,vwPhoneNumber,vwCompanyName,vwEmail,vwPassword,vwConfirmPassword,vwAuthenticationCode].forEach { (vw) in
            vw?.cornerRadius = (vw!.bounds.height)/2
            vw?.borderWidth = 1.0
            vw?.borderColors = UIColor.appthemeRedColor
            vw?.shadowColors = UIColor.lightGray
            vw?.shadowRadius = 2.0
            vw?.shadowOffset = CGSize(width: 1.0, height: 1.0)
            vw?.shadowOpacity = 0.8
        }
        
        txtCompanyName.rightPaddingView = 55
        txtfirstName.placeholder = getCommonString(key: "First_Name_key")
        txtLastName.placeholder = getCommonString(key: "Last_Name_key")
        txtPhoneNumber.placeholder = getCommonString(key: "Phone_Number_key")
        txtCompanyName.placeholder = getCommonString(key: "Company/Organization_key")
        txtEmail.placeholder = getCommonString(key: "Email_key")
        txtPassword.placeholder = getCommonString(key: "Password_key")
        txtConfirmPassword.placeholder = getCommonString(key: "Confirm_Password_key")
        txtAuthenticationCode.placeholder = getCommonString(key: "Authentication_Code_key")
        
        btnSignUp.setThemeButtonUI()
        btnSignUp.setTitle(getCommonString(key: "Sign_Up_key"), for: .normal)
        
        addDoneButtonOnKeyboard(textfield: txtPhoneNumber)
        addDoneButtonOnKeyboard(textfield: txtAuthenticationCode)
        
    }

    
}

//MARK: - IBAction method

extension AdminSignupVc
{
    
    @IBAction func btnSignUpTapped(_ sender: CustomButton)
    {
        
//        self.checkValidEmailDomain()
        if (txtfirstName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_first_name_key"))
        }
        else if (txtLastName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_last_name_key"))
        }
        else if (txtPhoneNumber.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_phone_number_key"))
        }
        else if (txtPhoneNumber.text?.isNumeric == false) || (txtPhoneNumber.text!.count < GlobalVariables.phoneNumberLimit)
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_valid_phone_number_key"))
        }
        else if (txtAuthenticationCode.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_authentication_code_key"))
        }
        else if (txtAuthenticationCode.text?.isNumeric == false)
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_valid_authentication_code_key"))
        }
//        else if (txtCompanyName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
//        {
//            makeToast(strMessage: getCommonString(key:"Please_select_company/Organizatino_key"))
//        }
        else if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_email_key"))
        }
//        else if (self.batchListCheck == false)
//        {
//            makeToast(strMessage: getCommonString(key: "Please_enter_valid_email"))
//        }
        else if !(isValidEmail(emailAddressString: txtEmail.text!))
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_valid_email"))
        }
        else if (txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_password_key"))
        }
        else if (txtPassword.text!.count) < 6
        {
            makeToast(strMessage: getCommonString(key:"Password_must_be_at_least_6_characters_long_key"))
        }
        else if (txtConfirmPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_confirm_password_key"))
        }
        else if(!(self.txtPassword.text == self.txtConfirmPassword.text))
        {
            makeToast(strMessage:getCommonString(key:"Password_and_confirmation_password_must_match_key"))
        }
        else
        {
            self.checkValidEmailDomain()
//            self.signUpAPICalling()
        }
        
    }
}

//MARK: - DropDown Selection method

extension AdminSignupVc
{
    
    func selectionIndex()
    {

        self.companyNameDD.selectionAction = { (index, item) in
            self.txtCompanyName.text = item
            self.view.endEditing(true)
            self.strCompnayID = (self.arrayCompanyName[index]["id"]).stringValue
            
            self.companyNameDD.hide()
            
        }
    }
}


//MARK: - TextField Delegate

extension AdminSignupVc:UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        if textField == txtPhoneNumber
        {
            let maxLength = GlobalVariables.phoneNumberLimit
            let currentString: NSString = txtPhoneNumber.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       
        if textField == txtCompanyName
        {
             self.view.endEditing(true)
            companyNameDD.show()
            return false
        }
        
        return true
    }
}

//MARK: - API calling

extension AdminSignupVc
{
    
    func signUpAPICalling()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(registerURL)"
            
            print("URL: \(url)")
            
            let param = ["lang" : GlobalVariables.strLang,
                         "email" : txtEmail.text ?? "",
                         "password" : txtPassword.text ?? "",
                         "mobile" : txtPhoneNumber.text ?? "",
                         "firstname" : txtfirstName.text ?? "",
                         "lastname" : txtLastName.text ?? "",
                         "timezone" : GlobalVariables.localTimeZoneName,
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? "",
                         "register_id" : Messaging.messaging().fcmToken ?? "",
                         "device_type" : GlobalVariables.deviceType,
                         "company_id" : strCompnayID,
                         "othorize_code" : txtAuthenticationCode.text ?? ""
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        
                        makeToast(strMessage: json["msg"].stringValue)
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                       // self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func getCompanyOrganizationList()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(adminListURL)"
            
            print("URL: \(url)")
            
            let param = [String:String]()
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
                        self.arrayCompanyName = data
                        
                        self.arrayCompanyNameString = data.map({"\($0["company_name"].stringValue)"})
                        self.companyNameDD.dataSource = self.arrayCompanyNameString
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        // self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func getCompanyBatchList()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(batchList)"
            
            print("URL: \(url)")
            
            let param = [String:String]()
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
                        self.arrayData = data
//                        self.arrayBatchList = data
                        
                        self.arrayBatchListString = data.map({"\($0["batch_name"].stringValue)"})

                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        // self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }

    
}
