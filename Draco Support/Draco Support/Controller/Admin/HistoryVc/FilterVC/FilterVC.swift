//
//  FilterVC.swift
//  Draco Support
//
//  Created by Haresh on 25/04/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import DropDown
import SwiftyJSON


protocol FilterDataDelegate {
    
    func filterData(technician_id: String, company_id: String, start_date: String, end_date: String)
    
}


class FilterVC: UIViewController, UITextFieldDelegate {

    //MARK: Outlets
    
    @IBOutlet weak var btnBackground: UIButton!
    
    @IBOutlet weak var vwFilter: CustomView!
    
    @IBOutlet weak var vwCompany: UIView!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var txtCompanyName: CustomTextField!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var txtStartDate: CustomTextField!
    @IBOutlet weak var txtEndDate: CustomTextField!
    
    @IBOutlet weak var lblTechnition: UILabel!
    @IBOutlet weak var txtTechnition: CustomTextField!
    
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var vwTechnition: UIView!
    
    @IBOutlet weak var vwCompanyHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var vwFilterHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var vwTechnicianHeightConstraints: NSLayoutConstraint!
    
    //MARK: Variable
    
    
    var companyNameDD = DropDown()
    var strCompnayID = ""
    
    var arrayCompanyName : [JSON] = []
    var arrayCompanyNameString = [String]()
    
    var technicianDD = DropDown()
    var strTechnitionID = ""
    
    var arrayTechnicianName : [JSON] = []
    var arrayTechnicianNameString = [String]()
    
    var selectedDate = Date()
    var isServiceDate = false
    
    var date = ""
    var serviceDate = ""
    
    var delegare : FilterDataDelegate?
    
    //MARK: - ViewController Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if getUserDetail("role") == "1" {
            
            vwTechnition.isHidden = false
            self.vwCompanyHeightConstraint.constant = 85.5
            self.vwTechnicianHeightConstraints.constant = 80.5
            
            getCompanyOrganizationList()
            getTechnicionList()
        }
        else {
            vwTechnition.isHidden = true
            vwCompany.isHidden = true
            
            self.vwFilterHeightConstrain.constant = 325 - 80.5 - 85.5
            self.vwTechnicianHeightConstraints.constant = 0
        }
        
        self.setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
        
    }

    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.companyNameDD, sender: self.txtCompanyName)
        self.configDD(dropdown: self.technicianDD, sender: self.txtTechnition)
        selectionIndex()
    }
    
}

//MARK:- Setup
extension FilterVC {
    
    func setUpUI() {
        
        lblCompanyName.text = getCommonString(key: "Company/Organization_key")
        lblTechnition.text  = getCommonString(key: "Technition_Key")
        lblDate.text        = getCommonString(key: "DaTe_Key")
        
        [lblCompanyName, lblDate, lblTechnition].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 16, fontname: .regular)
        }
        
        txtTechnition.leftPaddingView  = 55
        txtTechnition.rightPaddingView = 40
        
        txtStartDate.rightPaddingView  = 30
        txtEndDate.rightPaddingView    = 30
        
        txtCompanyName.leftPaddingView = 55
        txtCompanyName.placeholder = getCommonString(key: "Company/Organization_key")
        txtTechnition.placeholder = getCommonString(key: "Technition_Key")
        
        [txtTechnition, txtStartDate, txtEndDate, txtCompanyName].forEach { (txt) in
            
            txt?.delegate = self
//            txt?.isUserInteractionEnabled = false
            txt?.setThemeTextFieldUI()
        }
        
        [btnReset, btnSubmit].forEach { (btn) in
            btn?.setThemeButtonUI()
        }
    }
}

//MARK: - DropDown Selection method

extension FilterVC
{
    
    func selectionIndex()
    {
        
        self.companyNameDD.selectionAction = { (index, item) in
            self.txtCompanyName.text = item
            self.view.endEditing(true)
            self.strCompnayID = (self.arrayCompanyName[index]["id"]).stringValue
            
            self.companyNameDD.hide()
        }
        
        self.technicianDD.selectionAction = { (index, item) in
            self.txtTechnition.text = item
            self.view.endEditing(true)
            self.strTechnitionID = (self.arrayTechnicianName[index]["technician_id"]).stringValue
            
            self.technicianDD.hide()
        }
    }
}


// MARK:- Button Action

extension FilterVC {
    
    
    @IBAction func btnBackgroundAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnResetAction(_ sender: Any) {
        
        [txtCompanyName, txtStartDate, txtEndDate, txtTechnition].forEach { (txt) in
            txt?.text = ""
        }
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        delegare?.filterData(technician_id: self.strTechnitionID, company_id: self.strCompnayID, start_date: self.txtStartDate.text ?? "", end_date: self.txtEndDate.text ?? "")
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
}

extension FilterVC : DateTimePickerDelegate {
    
    func setDateandTime(dateValue: Date, type: Int) {
        
        if type == 1
        {
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = "dd-MM-yyyy"
            
            let dateFormatterForService = DateFormatter()
            dateFormatterForService.dateFormat = "yyyy-MM-dd hh:mm:ss"
            
            if self.isServiceDate
            {
                serviceDate = dateFormatterForService.string(from: dateValue)
                txtEndDate.text = dateformatter.string(from: dateValue)
            }
            else
            {
                selectedDate = dateValue
                date = dateFormatterForService.string(from: dateValue)
                txtStartDate.text = dateformatter.string(from: dateValue)
                txtEndDate.text = ""
            }
            
        }
        else if type == 0
        {
            
        }
    }
}


// MARK: - TextField Delegate

extension FilterVC {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtCompanyName
        {
            self.view.endEditing(true)
            companyNameDD.show()
            return false
        }
        
        if textField == txtTechnition {
            self.view.endEditing(true)
            technicianDD.show()
            return false
        }
        
        if textField == txtStartDate
        {
            self.view.endEditing(true)
            
            self.isServiceDate = false
            
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
            obj.pickerDelegate = self
            obj.controlType = 1
            obj.isSetMinimumDate = false
            obj.isSetMaximumDate = true
            obj.setMaximumDate = Date()
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            self.present(obj, animated: true, completion: nil)
            
            return false
        }
        else if textField == txtEndDate
        {
            
            if self.txtStartDate.text == "" {
                
                self.isServiceDate = true
                
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
                obj.pickerDelegate = self
                obj.controlType = 1
                obj.isSetMinimumDate = false
                obj.isSetMaximumDate = true
                obj.setMaximumDate = Date()
                obj.modalPresentationStyle = .overCurrentContext
                obj.modalTransitionStyle = .coverVertical
                self.present(obj, animated: true, completion: nil)
                
                return false
            }
            else {
                
                self.isServiceDate = true
                
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
                obj.pickerDelegate = self
                obj.controlType = 1
                obj.isSetMaximumDate = true
                obj.setMaximumDate = Date()
                obj.isSetMinimumDate = true
                obj.setMinimumDate = selectedDate
                obj.modalPresentationStyle = .overCurrentContext
                obj.modalTransitionStyle = .coverVertical
                self.present(obj, animated: true, completion: nil)
                
                return false
            }
            
            
            /*
            self.view.endEditing(true)
            if (txtStartDate.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
            {
                makeToast(strMessage: getCommonString(key:"Please_select_date_key"))
                return false
            }
            else
            {
                self.isServiceDate = true
                
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
                obj.pickerDelegate = self
                obj.controlType = 1
                obj.isSetMaximumDate = true
                obj.setMaximumDate = Date()
                obj.isSetMinimumDate = true
                obj.setMinimumDate = selectedDate
                obj.modalPresentationStyle = .overCurrentContext
                obj.modalTransitionStyle = .coverVertical
                self.present(obj, animated: true, completion: nil)
                
                return false
            }*/
            
        }
        
        return true
    }
}


// MARK:- API Calling

extension FilterVC {

    //Compnay List API Calling
    func getCompanyOrganizationList()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(adminListURL)"
            
            print("URL: \(url)")
            
            let param = [String:String]()
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
                        self.arrayCompanyName = data
                        
                        self.arrayCompanyNameString = data.map({"\($0["company_name"].stringValue)"})
                        self.companyNameDD.dataSource = self.arrayCompanyNameString
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        // self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    
    func getTechnicionList()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(technicianList)"
            
            print("URL: \(url)")
            
            let param = [String:String]()
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        print("Technition Name date: \(data)")
                        
                        self.arrayTechnicianName = data
                        self.arrayTechnicianNameString = data.map({"\($0["tec_firstname"]) \($0["tec_lastname"])"})
                        
                        self.technicianDD.dataSource = self.arrayTechnicianNameString
                        
                        print("technition name:- \(self.arrayTechnicianNameString)")
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        // self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    /*
    func filterDataApi(technician_id: String, company_id: String, start_date: String, end_date: String)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(reportURL)\(historyListURL)"
            
            print("URL: \(url)")
            
            let param = ["access_token" : getUserDetail("access_token"),
                         "user_id" : getUserDetail("id"),
                         "technician_id":"\(self.strTechnitionID)",
                "company_id":"\(self.strCompnayID)" ,
                "start_date": self.txtStartDate.text ?? "",
                "end_date":self.txtEndDate.text ?? ""
            ]

            //
            print("technitionId : \(self.strTechnitionID)")
            print("Data ID: \(self.strCompnayID)")
            
            print("param :\(param)")
            
            self.showLoader()
            
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
                        self.arrayFilterSubmit = data
                        
//                        self.arrayNewRequest = self.arrayRequestList.filter{$0["status"] == "todo"                }
//                        print("Arry New Request:-- \(self.arrayNewRequest)")
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
//                    else
//                    {
////                        self.strErrorMessage = json["msg"].stringValue
//                        makeToast(strMessage: json["msg"].stringValue)
//                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }*/
    
}
