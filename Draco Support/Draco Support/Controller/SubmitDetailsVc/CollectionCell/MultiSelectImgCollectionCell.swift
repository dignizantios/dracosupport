//
//  MultiSelectImgCollectionCell.swift
//  Draco Support
//
//  Created by YASH on 23/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class MultiSelectImgCollectionCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var btnAddPhoto: UIButton!
    @IBOutlet var btnRemoveImg: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        img.layer.cornerRadius = 5.0
        img.layer.masksToBounds = true
        btnRemoveImg.layer.cornerRadius = btnRemoveImg.frame.height/2
        //btnRemoveImg.layer.borderWidth = 1
        //btnRemoveImg.layer.borderColor = UIColor(red: 83.0/255.0, green: 108.0/255.0, blue: 207.0/255.0, alpha: 1.0).cgColor
        
        // Initialization code
    }

}
