//
//  Basicstuff.swift
//  Draco Support
//
//  Created by YASH on 22/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents
import NVActivityIndicatorView
import AlamofireSwiftyJSON
import SwiftyJSON
import Alamofire

struct GlobalVariables {
    
    static let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    static var localTimeZoneName: String { return TimeZone.current.identifier }
    static let deviceType = "1"
    static let strLang = "0"
    static let strSuccessResponse = "1"
    static let strAccessDenied = "-1"
    static let deviceToken = ""
    static let registerID = "1212"
    //MARK: - PhoneLength
    
    static let phoneNumberLimit = 10
    
    static let alreadyRegister = "Already Start This Issue by other Admin."
    
} 


let appdelgate = UIApplication.shared.delegate as! AppDelegate
let Defaults = UserDefaults.standard

//MARK: - Request List Flag

var isRequestList = Bool()
var isScanScreenForIssue = Bool()


let serverNotResponding = getCommonString(key: "Server_not_responding_Please_try_again_later_key")
let networkMsg = getCommonString(key: "No_internet_connection_Please_try_again_later_key")


//MARK: - Setup mapping

let StringFilePath = Bundle.main.path(forResource: "Language", ofType: "plist")
let dictStrings = NSDictionary(contentsOfFile: StringFilePath!)


func getCommonString(key:String) -> String
{
    return dictStrings?.object(forKey: key) as? String ?? ""
}


//MARK: - Storagae


func getUserDetail(_ forKey: String) -> String{
    guard let userDetail = UserDefaults.standard.value(forKey: "userDetails") as? Data else { return "" }
    let data = JSON(userDetail)
    return data[forKey].stringValue
}


//MARK: - Set Toaster

func makeToast(strMessage : String){
    
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager.show(messageSnack)
    
}


extension UIViewController
{
    
    //MARKL - Fonts
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    //MARK: - Alert
    
    func setAlert(msg:String)
    {
        let alert = UIAlertView()
        alert.title = "Draco Support"
        alert.message = msg
        alert.addButton(withTitle:"Ok")
        alert.show()
    }
    
    //MARK: - Logout API calling
    
    func logoutAPICalling()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(logoutURL)"
            
            print("URL: \(url)")
            
            let param = ["lang" : GlobalVariables.strLang,
                         "user_id" : getUserDetail("id"),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? "1212",
                         ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin: false, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        
                        Defaults.removeObject(forKey: "userDetails")
                        
                        let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AdminLoginVc") as! AdminLoginVc
                        let rearNavigation = UINavigationController(rootViewController: vc)
                        appdelgate.window?.rootViewController = rearNavigation
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
}



